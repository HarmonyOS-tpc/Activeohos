/*
 * Copyright (C) 2010 Michael Pardo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.activeharmony.sample;

import com.activeharmony.sample.slice.Details;

import com.activeharmony.query.Delete;

import org.junit.Test;

/**
 * DeleteTest Class
 */
public class DeleteTest extends SqlableTestCase {
    /**
     * testDelete checks DELETE
     */
    public void testDelete() {
        assertSqlEquals("DELETE ", new Delete());
    }

    /**
     * testFrom checks DELETE FROM
     */
    @Test
    public void testFrom() {
        assertSqlEquals("DELETE FROM Details", new Delete().from(Details.class));
    }
}
