/*
 * Copyright (C) 2010 Michael Pardo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.activeharmony.sample;

import com.activeharmony.ActiveHarmony;
import com.activeharmony.Cache;
import com.activeharmony.Configuration;
import com.activeharmony.annotation.Table;
import com.activeharmony.TableInfo;
import com.activeharmony.Model;

import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * CacheTest Class.
 */
public class CacheTest extends ExampleOhosTest {
    /**
     * setUp
     */
    protected void setUp() {
        Configuration configuration = new Configuration.Builder(getContext())
                .setDatabaseName("CacheTest")
                .addModelClasses(CacheTestModel.class, CacheTestModel2.class)
                .create();
        ActiveHarmony.initialize(configuration, true);
    }

    /**
     * testGetTableInfos
     */
    public void testGetTableInfos() {
        assertNotNull(Cache.getContext());
        Collection<TableInfo> tableInfos = Cache.getTableInfos();
        assertEquals(2, tableInfos.size());

        {
            TableInfo tableInfo = Cache.getTableInfo(CacheTestModel.class);
            assertNotNull(tableInfo);
            assertEquals("CacheTestModeOne", tableInfo.getTableName());
        }

        {
            TableInfo tableInfo = Cache.getTableInfo(CacheTestModel2.class);
            assertNotNull(tableInfo);
            assertEquals("CacheTestModelTwo", tableInfo.getTableName());
        }
    }

    @Table(name = "CacheTestModel")
    private static class CacheTestModel extends Model {
    }

    @Table(name = "CacheTestModel2")
    private static class CacheTestModel2 extends Model {
    }
}
