/*
 * Copyright (C) 2010 Michael Pardo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.activeharmony.sample;

import com.activeharmony.Configuration;
import com.activeharmony.Model;
import com.activeharmony.annotation.Table;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * ConfigurationTest Class.
 */
public class ConfigurationTest extends ExampleOhosTest {
    /**
     * validateDefaultValue
     *
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public void validateDefaultValue() throws IOException, ClassNotFoundException {
        Configuration configuration = new Configuration.Builder(getContext()).create();
        assertNotNull(configuration.getContext());
        assertEquals(1024, configuration.getCacheSize());
        assertEquals("Application.db", configuration.getDatabaseName());
        assertEquals(1, configuration.getDatabaseVersion());
        assertNull(configuration.getModelClasses());
        assertFalse(configuration.isValid());
        assertNull(configuration.getTypeSerializers());
        assertEquals(Configuration.SQL_PARSER_LEGACY, configuration.getSqlParser());
    }

    /**
     * testCreateConfigurationWithMockModel
     */
    public void testCreateConfigurationWithMockModel() {
        Configuration configuration = new Configuration.Builder(getContext())
                .addModelClass(SampleTestModel.class)
                .create();
        List<Class<? extends Model>> modelClasses = configuration.getModelClasses();
        assertEquals(1, modelClasses.size());
        assertTrue(configuration.isValid());
    }

    @Table(name = "SampleTestModel")
    private static class SampleTestModel extends Model {
    }
}
