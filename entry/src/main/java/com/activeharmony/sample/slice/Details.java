/*
 * Copyright (C) 2010 Michael Pardo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.activeharmony.sample.slice;

import com.activeharmony.Model;
import com.activeharmony.annotation.Column;
import com.activeharmony.annotation.Table;

/**
 * Details Model class
 */
@Table(name = "Details", id = "_id")
public class Details extends Model {
    @Column(name = "Age")
    private String age;

    @Column(name = "id")
    private long id;

    @Column(name = "Name")
    private String name;

    /**
     * Details Constaructor
     *
     */
    public Details() {
        super();
    }

    /**
     * Details Constaructor
     *
     * @param name name
     * @param age age
     */
    public Details(String name, String age) {
        super();
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getAge() {
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(String age) {
        this.age = age;
    }
}