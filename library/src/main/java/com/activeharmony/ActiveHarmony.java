/*
 * Copyright (C) 2010 Michael Pardo
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.activeharmony;

import com.activeharmony.util.Log;

import ohos.app.Context;

import net.sqlcipher.database.SQLiteDatabase;

/**
 * Function description
 * class ActiveHarmony
 */
public final class ActiveHarmony {
    // ////////////////////////////////////////////////////////////////////////////////////
    // PUBLIC METHODS
    // ////////////////////////////////////////////////////////////////////////////////////

    /**
     * method initialize
     *
     * @param context context
     */
    public static void initialize(Context context) {
        initialize(new Configuration.Builder(context).create());
    }

    /**
     * method initialize
     *
     * @param configuration configuration
     */
    public static void initialize(Configuration configuration) {
        initialize(configuration, false);
    }

    /**
     * method initialize
     *
     * @param context context
     * @param loggingEnabled loggingEnabled
     */
    public static void initialize(Context context, boolean loggingEnabled) {
        initialize(new Configuration.Builder(context).create(), loggingEnabled);
    }

    /**
     * method initialize
     *
     * @param configuration configuration
     * @param loggingEnabled loggingEnabled
     */
    public static void initialize(Configuration configuration, boolean loggingEnabled) {
        // Set logging enabled first
        setLoggingEnabled(loggingEnabled);
        Cache.initialize(configuration);
    }

    /**
     * method clearCache
     */
    public static void clearCache() {
        Cache.clear();
    }

    /**
     * method dispose
     */
    public static void dispose() {
        Cache.dispose();
    }

    /**
     * method setLoggingEnabled
     *
     * @param enabled enabled
     */
    public static void setLoggingEnabled(boolean enabled) {
        Log.setEnabled(enabled);
    }

    /**
     * method getDatabase
     *
     * @return SQLiteDatabase
     */
    public static SQLiteDatabase getDatabase() {
        return Cache.openDatabase();
    }

    /**
     * method beginTransaction
     */
    public static void beginTransaction() {
        Cache.openDatabase().beginTransaction();
    }

    /**
     * method endTransaction
     */
    public static void endTransaction() {
        Cache.openDatabase().endTransaction();
    }

    /**
     * method setTransactionSuccessful
     */
    public static void setTransactionSuccessful() {
        Cache.openDatabase().setTransactionSuccessful();
    }

    /**
     * method inTransaction
     *
     * @return inTransaction status
     */
    public static boolean inTransaction() {
        return Cache.openDatabase().inTransaction();
    }

    /**
     * method execSQL
     *
     * @param sql sql
     */
    public static void execSQL(String sql) {
        Cache.openDatabase().execSQL(sql);
    }

    /**
     * method execSQL
     *
     * @param sql sql
     * @param bindArgs bindArgs
     */
    public static void execSQL(String sql, Object[] bindArgs) {
        Cache.openDatabase().execSQL(sql, bindArgs);
    }
}
